let nconf = require('nconf'),
  config,
  overrides = {},
  pack = require('../package'),
  repoInfo = require('../repo-info'),
  joi = require('joi'),
  ConfigSchema = require('../lib/schema/database/config-schema'),
  ConfigAccessor = require('ecom-base-lib').ConfigAccessor,
  _ = require('lodash'),
  path = require('path'),
  co = require('co'),
  winston = require('winston'),
  BaseHelper =  require('ecom-base-lib').BaseHelper;

class BaseDataFix extends BaseHelper {
  constructor() {
    super();
    this.dependencies = {};
    this.config = {};
    this.repoConfig = {};
  }

  init() {
    const me = this;
    return co(function* () {
      yield me._setConfig();
      me._setDependencies();
    });
  }

  _setConfig() {
    const me = this;
    return co(function* () {

      // Set the override configs
      nconf.argv().env();
      if (nconf.get('overrideConfig')) {
        overrides = require(nconf.get('overrideConfig'));
        nconf.overrides(overrides);
      }
      nconf.defaults(require('../config/config'));
      config = nconf.get();

      let winstonLogger = new (winston.Logger)({
        exitOnError: false,
        transports: [
          new winston.transports.Console(),
          new winston.transports.DailyRotateFile({
            name: 'file',
            datePattern: config.server.logging.datePattern,
            filename: path.join(config.server.logging.logsDirectory, config.server.logging.dataFixLogName),
            level: 'debug',
            maxsize: 1024 * 1024 * 10
          })
        ]
      });

      // Set logger to winston
      me.dependencies.logger = winstonLogger;

      let configAccessor = new ConfigAccessor(me.dependencies, config);
      yield configAccessor.init();
      let result = configAccessor.configKeyMap[config.environment];
      nconf.set(pack.name, _.merge(config[pack.name], result));
      config = nconf.get();
      repoInfo.updateRepoAutoConfig(me.repoConfig);
      me.repoConfig = me.repoConfig.config;
      if (ConfigSchema.ServiceConfigSchema) {
        let validationResult = joi.validate(me.repoConfig, ConfigSchema.ServiceConfigSchema, { abortEarly: false, allowUnknown: true });
        if (validationResult.error) {
          /*eslint-disable*/
          console.log(`Config error:\n ${validationResult.error}`);
          /*eslint-enable*/
          me.errorv2('BaseDataFix', '_setConfig', validationResult.error);
          process.exit(1);
        }
      }
    });
  }

  // Deprecated. Use the Data Access classes instead.
  // Uncomment this when you want to run a PSQL query 
  // (Do this only when the required functionality is not exposed in Posty)
  // executePostgresCommand (command, connectionString) {
  //   let pg = require('co-pg')(require('pg'));
  //   const me = this;
  //   return co(function *() {
  //     let client = new pg.Client(connectionString);
  //     yield client.connectPromise();
  //     let result = yield client.queryPromise(command);
  //     client.end();
  //     return result;
  //   }).catch((err) => {
  //     me.errorv2('BaseDataFix', 'executeCommand', err);
  //     throw err;
  //   });
  // }

  _setDependencies() {
    //Initialize any data-accessors here.
  }
}

module.exports = BaseDataFix;
