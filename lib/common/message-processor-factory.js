'use strict';

const BaseHelper = require('ecom-base-lib').BaseHelper,
  AuditMessageProcessor = require('../worker/audit-message-processor');

class MessageProcessorFactory extends BaseHelper {
  constructor (dependencies, config) {
    super(dependencies, config);
    this.dependencies = dependencies;
    this.config = config;
  }
  getProcessorInstance (queue, processRequestContext) {
    let me = this;
    switch (queue) {
    case 'audit-service':
      return new AuditMessageProcessor(me.dependencies, me.config, processRequestContext);
    default:
      me.log('MessageProcessorFactory', 'getProcessorInstance', 'InvalidQueue', queue);
    }
  }
}

module.exports = MessageProcessorFactory;
