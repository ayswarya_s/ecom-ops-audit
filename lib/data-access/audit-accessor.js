"use strict";

const EntityAccessor = require('ecom-base-lib').EntityAccessor,
  joi = require('joi'),
  co = require('co'),
  _ = require('lodash');

const options = {
  pk: 'id',
  indexFields: [
  ],
  tableName : 'SampleAudit',
  schema: joi.object({
    id: joi.string().required(),
    entity_id: joi.string().required(),
    entity_type: joi.string().required(),
    timestamp: joi.date().required(),
    user_id: joi.string().required(),
    job_status: joi.string().required().valid("Created", "Queuing", "Queued", "QueueAttemptFailed", "Processing", "Failed", "FailedRetryable", "Success").default("Created"),
    record_version: joi.number().integer().required().default(0),
    processor_hostname: joi.string().description("Name of the host processing the joi").optional()
  }).unknown(true),
  version_field_name: 'record_version'
};

/**
* This is the accessor that persist carrier cart data.
*/
class AuditAccessor extends EntityAccessor {
  /*
  * Constructor
  * @param {object} dependencies The object that contains the logger etc.
  * @param {object} config The base config object that has the rethinkdb config.
  */
  constructor(dependencies, config, requestContext) {
    super(options, dependencies, config, false, requestContext);
  }

  getByStatus (status) {
    let me = this;
    return co(function * () {
      try {
        return yield me.filter({"job_status": status}, true)
      } catch (e) {
        console.log(e);
        throw e;
      }
    });
  }

}

module.exports = AuditAccessor;
