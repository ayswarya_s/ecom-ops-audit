'use strict';

const repoInfo = require('../../repo-info'),
  RouteHandler = require('./route-handler');


class RegisterPrivateRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.repoConfig = {};
    repoInfo.updateRepoAutoConfig(this.repoConfig);
    this.repoConfig = this.repoConfig.config;
    this.RouteHandler = new RouteHandler(dependencies, this.repoConfig);
    this.config.appId = this.repoConfig.appId = repoInfo.name;
  }

  registerRoutes(server) {
    server.log('No private routes to register for Blueprint service');
  }
}

module.exports = RegisterPrivateRoutes;
