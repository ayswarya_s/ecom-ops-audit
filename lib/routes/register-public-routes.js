'use strict';

const repoInfo = require('../../repo-info'),
  RouteHandler = require('./route-handler'),
  Schema = require('ecom-fabric-lib').Schema.AuthHeaderSchema,
  LogSchema = require('../schema/audit/log-schema');


class RegisterPublicRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.repoConfig = {};
    repoInfo.updateRepoAutoConfig(this.repoConfig);
    this.repoConfig = this.repoConfig.config;
    this.routeHandler = new RouteHandler(dependencies, this.repoConfig);
    this.config.appId = this.repoConfig.appId = repoInfo.name;
  }

  registerRoutes(server) {
    const me = this;
    server.log('Registering public routes for Blueprint service');

    server.route({
      method: 'POST',
      path: '/v1/audit/',
      config: {
        handler: (request, reply) => me.routeHandler.pushLog(request, reply),
        description: 'Post route',
        tags: ['api', 'audit'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          payload: LogSchema.EntityLogArray,
          headers: Schema.AuthHeader
        }
      }
    });
  }
}
module.exports = RegisterPublicRoutes;
