'use strict'

const BaseHelper = require('ecom-base-lib').BaseHelper,
  repoInfo = require('../../repo-info'),
  AuditLogService = require('../service/audit-log-service');

class RouteHandler extends BaseHelper {
  constructor(dependencies, config) {
    super(dependencies, config);
    this.config = config;
    this.config.appId = repoInfo.name;
  }

  pushLog(request, reply) {
    let me = this;
    let service = new AuditLogService(me.dependencies, me.config, request);
    service.createLog(request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }

  getAllLogs(request, reply) {
    let me = this;
    let service = new AuditLogService(me.dependencies, me.config, request);
    service.getAllLogs(request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
}

module.exports = RouteHandler;
