const joi = require('joi');

const EntityLogSchema = joi.object({
  entity_id: joi.string().required(),
  entity_type: joi.string().required().valid("product", "user", "order"),
  entity_data: joi.object().required(),
  user_id: joi.string().required(),
  timestamp: joi.date().required()
}).required();

const EntityLogArray = joi.array().items(EntityLogSchema).min(1).required();

module.exports = {
  EntityLogArray
}
