'use strict'

const BaseHelper = require('ecom-base-lib').BaseHelper,
  AuditAccessor = require('../data-access/audit-accessor'),
  MessageProducer = require('ecom-jobs-lib').Messaging.MessageProducer,
  co = require('co'),
  _ = require('lodash');

class AuditLogService extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.auditAccessor = new AuditAccessor(dependencies, config, requestContext);
    this.messageProducer = new MessageProducer(dependencies, config, requestContext);
  }

  createLog(payload) {
    let me = this;
    return co(function* () {
      try {
        let auditLog = yield me.auditAccessor.saveAll(payload);
        return auditLog;
      }
      catch (err) {
        me.error("AuditService", "pushLog", "err");
        throw err;
      }
    });
  }

  getAllLogs() {
    let me = this;
    return co(function*() {
      try {
        return yield me.auditAccessor.get();
      } catch(err) {
        me.error("AuditService", "getAllLogs", "err");
        throw err;
      }
    });
  }

}

module.exports = AuditLogService;
