"use strict";

const BaseMessageProcessor = require('ecom-jobs-lib').Messaging.BaseMessageProcessor,
  ElasticSearchClient = require('ecom-fabric-lib').Clients.ElasticSearchClient,
  AuditAccessor = require('../data-access/audit-accessor'),
  repoInfo = require('../../repo-info'),

  co = require('co'),
  os = require('os');
  // Errors = require('../errors/errors.json');

class AuditMessageProcessor extends BaseMessageProcessor {
  constructor (dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.repoConfig = repoInfo.getRepoAutoConfig(config);
    this.auditAccessor = new AuditAccessor(dependencies, this.repoConfig, requestContext);
    this.elasticSearchClient = new ElasticSearchClient(dependencies, this.repoConfig);
  }

  generateNewRequestContext(payload) {
    return {auth: {credentials: payload.message.credentials}, headers: {}};
  }

  processMessage (payload, ack, unitTest) {
    let me = this
    let activationJob;
    return co(function * () {
      let carrierAdapterService;
      try {
        let err;
        let message = payload.message;
        let auditLog = yield me.auditAccessor.update(message.id, {job_status: "Processing", processor_hostname: os.hostname()});
        if (message.entity_type == 'product') {

        }
        try {
          let esResult = yield me.elasticSearchClient.createDocument('audit', message.entity_type, message);
          if (esResult.created) {
            auditLog.job_status = "Success";
          } else {
            auditLog.job_status = "FailedRetryable";
          }
        } catch (error) {
          err = error;
          auditLog.job_status = "FailedRetryable";
        }
        try {
          yield me.auditAccessor.versionedWrite(auditLog);
        } catch (err2) {
          console.log(err2);
        }
        ack();
      } catch (e) {
        throw e;
      }
    });
  }

  onFailure (payload, channel, exchange, ack) {
    let me = this
    return co(function * () {
      try {
        let message = payload.message;
        let auditLog = yield me.auditAccessor.get(message.id);
        me.log("ActivationMessageProcessor", "onRetryFailed", "Saving to DB since all retries failed", payload);
        auditLog.job_status = "Failed";
        yield me.auditAccessor.versionedWrite(auditLog);
        ack();
      } catch (e) {
        me.error('ActivationMessageProcessor', 'onRetryFailed', e, payload);
      }
    });
  }
}

module.exports = AuditMessageProcessor;
