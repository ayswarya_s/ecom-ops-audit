'use strict'
const MessageProcessorFactory = require('../common/message-processor-factory'),
  WorkerBootstrap = require('ecom-jobs-lib').Worker.WorkerBootstrap,
  co = require('co'),
  path = require('path');

return co(function*() {
  try {
    const workerBootstrap = new WorkerBootstrap('./config/config.json', path.join(__dirname, '../..'));
    yield workerBootstrap.bootstrap();

    const messageProcessorFactory = new MessageProcessorFactory(workerBootstrap.dependencies, workerBootstrap.config);

    workerBootstrap.startWorker(messageProcessorFactory);
    /*eslint-disable */
    console.log('Worker started...');
  } catch (e) {
    console.log('An error occurred');
    console.log(e);
    /*eslint-enable */
    process.exit();
  }
});
