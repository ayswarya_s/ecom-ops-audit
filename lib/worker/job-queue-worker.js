'use strict'

const CronWorker = require('ecom-jobs-lib').BaseCronWorker,
  MessageProducer = require('ecom-jobs-lib').Messaging.MessageProducer,
  AuditAccessor = require('../data-access/audit-accessor'),
  co = require('co'),
  _ = require('co-lodash');

class AuditWorker extends CronWorker {
  constructor(dependencies, config){
    config.cronTime = '*/2 * * * *'
    config.runOnInit = true
    super(dependencies, config);
    this.auditAccessor = new AuditAccessor(dependencies, config);
    this.messageProducer = new MessageProducer(dependencies, config);
  }

  job () {
    let me = this;
    return co(function* () {
      try {
        let jobsToQueue = yield me.auditAccessor.getByStatus('Created');
        if (jobsToQueue.length > 0) {
          me._queueJob(jobsToQueue);
        }
      } catch(err) {
          console.log(err);
          throw err;
      }
    });
  }

  _queueJob(allJobs) {
    let me = this;
    return co(function* () {
      yield* _.coEach(allJobs, function* (job) {
        try {
          yield me.messageProducer.sendMessage("audit-service", job, 0, "audit-service");
        } catch (queueError) {
            job.job_status = "QueueAttemptFailed";
        }
        job.job_status = "Queued";
        try {
          me.auditAccessor.versionedWrite(job);
        } catch (error) {
          console.log(error);
          throw error;
        }
      });
    });
  }
}


module.exports = AuditWorker
